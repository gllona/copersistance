# Copersistance

Copersistance is a RDBMS persistance library for Java 8+ inspired on JPA.

You define entities and repositories, and use the available repository methods to do DB
operations on the entities.

Copersistance uses `sql2o` for all underlying operations on the DB.

A simple connection pool is implemented to avoid opening a connection with each DB operation. 

## Limitations

* Works only with MySQL. Could work with other RDBMS's but haven't been tested
* Entities must have either a single (Java) Long primary key, or a composite primary key made
  by two Long's
* The code is in `alpha` stage. Use for production at your own risk.
* Code is not unit tested

## How to use

Documentation is to come.

Meanwhile, see how it is used in https://gitlab.com/gllona/talks

## What you need

* Java 1.8
* A MySQL server

### Dependencies

* `org.apache.commons:commons-lang3:3.9`
* `org.sql2o:sql2o:1.6.0`
* `com.google.code.gson:gson:2.8.5`
* `org.slf4j:slf4j-log4j12:1.7.30`
* `log4j:log4j:1.2.17`
