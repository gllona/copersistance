package org.logicas.librerias.copersistance;

public interface CopersistenceConfig {

    String getRdbms();

    String getHost();

    int getPort();

    String getDatabase();

    String getUsername();

    String getPassword();

    String getOptions();

    int getConnectionPoolSize();

    int getGetTimeoutInMillis();

    int getConnTtlInMillis();

    String getKeepAliveQuery();

}
