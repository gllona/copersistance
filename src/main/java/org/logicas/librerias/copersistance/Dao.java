package org.logicas.librerias.copersistance;

import java.io.Closeable;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReferenceArray;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.copersistance.exceptions.CopersistenceException;
import org.sql2o.Connection;
import org.sql2o.Sql2o;

public class Dao {

    public class DaoConnection implements Closeable {

        private Connection conn;
        private long lastKeepAliveTimestamp;
        private boolean isTransactional;
        private boolean gotTransactional;
        private boolean shouldCommit;

        DaoConnection() {
            openConn(false);
        }

        public synchronized Connection get() {
            if (isTransactional && ! gotTransactional) {
                close();
                openConn(true);
            } else if (shouldKeepAlive()) {
                doKeepAlive();
            }
            return conn;
        }

        public void commit() {
            if (! isTransactional) {
                throw new CopersistenceException("Can not commit non-transactional connections");
            }
            shouldCommit = true;
        }

        public void rollback() {
            if (! isTransactional) {
                throw new CopersistenceException("Can not rollback non-transactional connections");
            }
            shouldCommit = false;
        }

        @Override
        public void close() {
            if (gotTransactional) {
                closeConn();
                openConn(false);
            }
            release(this);
        }

        private void openConn(boolean transactional) {
            if (transactional) {
                conn = sql2o.beginTransaction();
                isTransactional = true;
                gotTransactional = true;
                shouldCommit = false;
            } else {
                conn = sql2o.open();
                isTransactional = false;
                gotTransactional = false;
            }
            lastKeepAliveTimestamp = System.currentTimeMillis();
            shouldCommit = false;
        }

        private void closeConn() {
            if (conn == null) {
                return;
            }
            if (isTransactional) {
                if (shouldCommit) {
                    conn.commit();
                } else {
                    conn.rollback();
                }
            }
            conn.close();
            gotTransactional = false;
            isTransactional = false;
            conn = null;
        }

        private boolean shouldKeepAlive() {
            long now = System.currentTimeMillis();
            return now - lastKeepAliveTimestamp > connTtlInMillis;
        }

        private void doKeepAlive() {
            if (! connIsAlive()) {
                try {
                    closeConn();
                } catch (Exception ignored) {
                }
                openConn(false);
            }
        }

        private boolean connIsAlive() {
            try {
                conn.createQuery(keepAliveQuery).executeAndFetchTable();
                return true;
            } catch (Exception e) {
                return false;
            }
        }

        public void setTransactional() {
            gotTransactional = isTransactional;
            isTransactional = true;
        }
    }

    private static Logger logger = LogManager.getLogger(Dao.class);

    private static DaoConnection NULL_CONNECTION = null;
    private static final Object nullConnectionCreatorMonitor = new Object();

    private final Sql2o sql2o;
    private final int poolSize;
    private final int timeoutInMillis;
    private final int connTtlInMillis;
    private final String keepAliveQuery;
    private final AtomicReferenceArray<DaoConnection> conns;
    private final ArrayBlockingQueue<DaoConnection> freeConns;

    Dao(Sql2o sql2o, int connPoolSize, int timeoutInMillis, int connTtlInMillis, String keepAliveQuery) {
        this.sql2o = sql2o;
        this.poolSize = connPoolSize;
        this.timeoutInMillis = timeoutInMillis;
        this.connTtlInMillis = connTtlInMillis;
        this.keepAliveQuery = keepAliveQuery;
        this.conns = new AtomicReferenceArray<>(connPoolSize);
        this.freeConns = new ArrayBlockingQueue<>(connPoolSize);
        createNullConnectionMarker();
        fillPool();
    }

    private void createNullConnectionMarker() {
        synchronized (nullConnectionCreatorMonitor) {
            if (NULL_CONNECTION == null) {
                NULL_CONNECTION = new DaoConnection();
            }
        }
    }

    public DaoConnection nullConn() {
        return NULL_CONNECTION;
    }

    private synchronized void fillPool() {
        try {
            for (int i = 0; i < poolSize; i++) {
                conns.set(i, new DaoConnection());
            }
        } catch (Exception e) {
            throw new CopersistenceException("Could not initialize DB connection pool. Check DB params or try decreasing pool size", e);
        }

        for (int i = 0; i < poolSize; i++) {
            try {
                freeConns.put(conns.get(i));
            } catch (InterruptedException e) {
                throw new CopersistenceException("Could not add DB connection to pool (timeout)", e);
            }
        }
    }

    public DaoConnection take(boolean transactional) {
        try {
            DaoConnection conn = freeConns.poll(timeoutInMillis, TimeUnit.MILLISECONDS);
            if (conn != null && transactional) {
                conn.setTransactional();
            }
            return conn;
        } catch (InterruptedException e) {
            throw new CopersistenceException("Unable to take a DB connection", e);
        }
    }

    public void release(DaoConnection conn) {
        if (conn == null || freeConns.contains(conn)) {
            return;
        }
        try {
            freeConns.put(conn);
        } catch (InterruptedException e) {
            throw new CopersistenceException("Unable to release a DB connection", e);
        }
    }

    public synchronized void shutdown() {
        freeConns.clear();
        for (int i = 0; i < poolSize; i++) {
            try {
                conns.get(i).closeConn();
            } catch (Exception e) {
                logger.warn(String.format("Shutdown: unable to close DB connection %d", i));
            }
        }
    }
}
