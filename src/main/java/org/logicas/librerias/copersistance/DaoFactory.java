package org.logicas.librerias.copersistance;

import org.logicas.librerias.copersistance.exceptions.CopersistenceException;
import org.logicas.librerias.copersistance.sql2o.LocalDateConverter;
import org.logicas.librerias.copersistance.sql2o.LocalDateTimeConverter;
import org.sql2o.Sql2o;
import org.sql2o.converters.Converter;
import org.sql2o.quirks.NoQuirks;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DaoFactory {

    private static List<String> supportedRdbms = List.of("mysql", "mariadb");

    private static DaoFactory instance;

    public static synchronized DaoFactory getInstance() {
        if (instance == null) {
            instance = new DaoFactory();
        }
        return instance;
    }

    public synchronized Dao createDao(CopersistenceConfig config) {
        assertRdbmsIsSupported(config.getRdbms());
        // Sql2o sql2o = new Sql2o(buildSql2oUrl(config), config.getUsername(), config.getPassword());
        Sql2o sql2o = createSql2o(config);
        return new Dao(
            sql2o,
            config.getConnectionPoolSize(),
            config.getGetTimeoutInMillis(),
            config.getConnTtlInMillis(),
            config.getKeepAliveQuery()
        );
    }

    private Sql2o createSql2o(CopersistenceConfig config) {
        final Map<Class, Converter> mappers = new HashMap<>();
        mappers.put(LocalDate.class, new LocalDateConverter());
        mappers.put(LocalDateTime.class, new LocalDateTimeConverter());
        Sql2o sql2o = new Sql2o(buildSql2oUrl(config), config.getUsername(), config.getPassword(), new NoQuirks(mappers));
        return sql2o;
    }

    private void assertRdbmsIsSupported(String rdbms) {
        if (! supportedRdbms.contains(rdbms)) {
            throw new CopersistenceException(
                String.format("Rdbms [%s] not supported", rdbms)
            );
        }
    }

    private String buildSql2oUrl(CopersistenceConfig config) {
        return String.format(
            "jdbc:%s://%s:%d/%s%s",
            config.getRdbms(),
            config.getHost(),
            config.getPort(),
            config.getDatabase(),
            config.getOptions().isEmpty() ? "" : "?" + config.getOptions()
        );
    }
}
