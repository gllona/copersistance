package org.logicas.librerias.copersistance.entities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;

import java.util.Map;

public abstract class BaseEntity {

    protected static Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        //.setPrettyPrinting()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .serializeNulls()
        .create();

    protected static final Object NULL_TAG = new Object();

    public abstract String[] getKeyColumns();

    public abstract Map<String, Object> parameters();

    public abstract String retrieveCacheId();

    protected boolean shouldPersist(Object member) {
        return NULL_TAG == member ? true : (member != null);
    }

    protected Object persistAs(Object member) {
        return NULL_TAG == member ? null : member;
    }

    protected void conditionalAddToParams(Object attribute, String field, Map<String, Object> params) {
        if (shouldPersist(attribute)) {
            params.put(field, persistAs(attribute));
        }
    }
}
