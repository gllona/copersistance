package org.logicas.librerias.copersistance.entities;

public abstract class DualKeyEntity extends BaseEntity {

    public abstract Long getId1();

    public abstract Long getId2();

    public abstract void setIds(Long id1, Long id2);

    @Override
    public String retrieveCacheId() {
        return String.valueOf(getId1()) + "|" + String.valueOf(getId2());
    }
}
