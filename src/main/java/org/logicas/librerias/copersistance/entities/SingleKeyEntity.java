package org.logicas.librerias.copersistance.entities;

public abstract class SingleKeyEntity extends BaseEntity {

    public abstract Long getId();

    public abstract void setId(Long id);

    @Override
    public String retrieveCacheId() {
        return String.valueOf(getId());
    }
}
