package org.logicas.librerias.copersistance.example;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.logicas.librerias.copersistance.entities.DualKeyEntity;

@Setter
@Getter
public class SampleDualKeyEntity extends DualKeyEntity {

    public static final String FIELD_ID_1 = "id1";
    public static final String FIELD_ID_2 = "id2";
    public static final String FIELD_DESCRIPTION = "description";

    public static final String[] FIELDS = {
        FIELD_ID_1,
        FIELD_ID_2,
        FIELD_DESCRIPTION
    };

    private static final String[] KEYS = {
        FIELD_ID_1,
        FIELD_ID_2
    };

    private Long id1;
    private Long id2;
    private String description;

    @Override
    public Long getId1() {
        return id1;
    }

    @Override
    public Long getId2() {
        return id2;
    }

    @Override
    public void setIds(Long id1, Long id2) {
        this.id1 = id1;
        this.id2 = id2;
    }

    @Override public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id1, FIELD_ID_1, params);
        conditionalAddToParams(id2, FIELD_ID_2, params);
        conditionalAddToParams(description, FIELD_DESCRIPTION, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

//    @Override
//    public String toString() {
//        return toJsonObject().toString();
//    }
//
//    public JSONObject toJsonObject() {
//        return new JSONObject()
//            .put(FIELD_ID_1, id1)
//            .put(FIELD_ID_2, id2)
//            .put(FIELD_DESCRIPTION, description);
//    }
}
