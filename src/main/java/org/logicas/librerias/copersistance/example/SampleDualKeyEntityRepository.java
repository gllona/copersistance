package org.logicas.librerias.copersistance.example;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.DualKeyEntityRepository;

public class SampleDualKeyEntityRepository extends DualKeyEntityRepository<SampleDualKeyEntity> {

    private static SampleDualKeyEntityRepository instance;

    private Dao dao;

    private SampleDualKeyEntityRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized SampleDualKeyEntityRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new SampleDualKeyEntityRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "sample_dual_key_entity_table";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return SampleDualKeyEntity.FIELDS;
    }

    @Override
    public String id1FieldName() {
        return SampleDualKeyEntity.FIELD_ID_1;
    }

    @Override
    public String id2FieldName() {
        return SampleDualKeyEntity.FIELD_ID_2;
    }
}
