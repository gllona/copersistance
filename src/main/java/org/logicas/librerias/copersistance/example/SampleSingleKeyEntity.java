package org.logicas.librerias.copersistance.example;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

@Setter
@Getter
public class SampleSingleKeyEntity extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_DESCRIPTION = "description";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_DESCRIPTION
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String description;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(description, FIELD_DESCRIPTION, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

//    @Override
//    public String toString() {
//        return toJsonObject().toString();
//    }
//
//    public JSONObject toJsonObject() {
//        return new JSONObject()
//            .put(FIELD_ID, id)
//            .put(FIELD_DESCRIPTION, description);
//    }
}
