package org.logicas.librerias.copersistance.example;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class SampleSingleKeyEntityRepository extends
    SingleKeyEntityRepository<SampleSingleKeyEntity> {

    private static SampleSingleKeyEntityRepository instance;

    private Dao dao;

    private SampleSingleKeyEntityRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized SampleSingleKeyEntityRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new SampleSingleKeyEntityRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "sample_single_key_entity_table";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return SampleSingleKeyEntity.FIELDS;
    }

    @Override
    public String idFieldName() {
        return SampleSingleKeyEntity.FIELD_ID;
    }
}
