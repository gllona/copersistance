package org.logicas.librerias.copersistance.exceptions;

public class CopersistenceException extends RuntimeException {

    public CopersistenceException(String message) {
        super(message);
    }

    public CopersistenceException(String message, Throwable t) {
        super(message, t);
    }
}
