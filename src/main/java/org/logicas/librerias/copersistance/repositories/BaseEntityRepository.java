package org.logicas.librerias.copersistance.repositories;

import java.lang.reflect.ParameterizedType;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.Dao.DaoConnection;
import org.logicas.librerias.copersistance.entities.BaseEntity;
import org.logicas.librerias.copersistance.exceptions.CopersistenceException;
import org.sql2o.Query;

import static org.logicas.librerias.copersistance.repositories.BaseEntityRepository.OrderBy.*;

public abstract class BaseEntityRepository<T extends BaseEntity> {

    public enum OrderBy {
        ASC,
        DESC,
        UNORDERED
    }

    public enum Aggregation {
        COUNT,
        SUM,
        AVG,
        STDDEV,
        VARIANCE,
        MAX,
        MIN
    }

    public static class AggregationResult<T> {
        public T groupByValue;
        public long funResult;
    }

    public static class IntegerAggregationResult extends AggregationResult<Integer> {
    }

    public static class LongAggregationResult extends AggregationResult<Long> {
    }

    public static class StringAggregationResult extends AggregationResult<String> {
    }

    protected abstract Dao dao();

    protected abstract String tableName();

    protected abstract String[] columnNames();

    public List<T> readAll() {
        return readAll(null);
    }

    public List<T> readAll(DaoConnection conn) {
        return readByCondition(conn, "TRUE");
    }

    public List<T> readAllByPage(int page, int quantityPerPage) {
        return readAllByPage(null, page, quantityPerPage);
    }

    public List<T> readAllByPage(DaoConnection conn, int page, int quantityPerPage) {
        return readAllByPage(conn, page, quantityPerPage, null, UNORDERED, null);
    }

    public List<T> readAllByPage(int page, int quantityPerPage, String orderByFieldName, OrderBy orderBy) {
        return readAllByPage(null, page, quantityPerPage, orderByFieldName, orderBy);
    }

    public List<T> readAllByPage(DaoConnection conn, int page, int quantityPerPage, String orderByFieldName, OrderBy orderBy) {
        return readAllByPage(conn, page, quantityPerPage, orderByFieldName, orderBy, null);
    }

    public List<T> readAllByPage(int page, int quantityPerPage, String condition) {
        return readAllByPage(null, page, quantityPerPage, condition);
    }

    public List<T> readAllByPage(DaoConnection conn, int page, int quantityPerPage, String condition) {
        return readAllByPage(conn, page, quantityPerPage, null, UNORDERED, condition);
    }

    public List<T> readAllByPage(int page, int quantityPerPage, String orderByFieldName, OrderBy orderBy, String condition) {
        return readAllByPage(null, page, quantityPerPage, orderByFieldName, orderBy, condition);
    }

    public List<T> readAllByPage(DaoConnection conn, int page, int quantityPerPage, String orderByFieldName, OrderBy orderBy, String condition) {
        String suffix =
            (orderBy == UNORDERED ? "" : " ORDER BY " + orderByFieldName + " " + orderBy.name()) +
            " LIMIT " + quantityPerPage + " OFFSET " + ((page - 1) * quantityPerPage);
        return readByCondition(conn, (condition == null ? "TRUE" : "(" + condition + ")") + suffix);
    }

    public int countAll(String condition) {
        return countAll(null, condition);
    }

    public int countAll(DaoConnection conn, String condition) {
        String SQL = QueryBuilder.count(tableName(), condition);
        if (conn == null) {
            try (DaoConnection conn2 = dao().take(false)) {
                return doCountAll(conn2, SQL);
            }
        } else {
            return doCountAll(conn, SQL);
        }
    }

    private int doCountAll(DaoConnection conn, String SQL) {
        return conn.get().createQuery(SQL).executeAndFetchFirst(Integer.class);
    }

    public int aggregate(Aggregation fun, String fieldName, String condition) {
        return aggregate(null, fun, fieldName, condition);
    }

    public int aggregate(DaoConnection conn, Aggregation fun, String fieldName, String condition) {
        String SQL = QueryBuilder.simpleAggregate(fun.name(), tableName(), fieldName, condition);
        if (conn == null) {
            try (DaoConnection conn2 = dao().take(false)) {
                return doAggregate(conn2, SQL);
            }
        } else {
            return doAggregate(conn, SQL);
        }
    }

    private int doAggregate(DaoConnection conn, String SQL) {
        return conn.get().createQuery(SQL).executeAndFetchFirst(Integer.class);
    }

    public List<IntegerAggregationResult> aggregateIntegerGroupBy(Aggregation fun, String columnName, String condition,
                                                                  String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                                                  Integer limit, Integer offset) {
        return aggregateIntegerGroupBy(null, fun, columnName, condition, groupByColumnName, having, orderBy, orderByAscDesc, limit, offset);
    }

    public List<IntegerAggregationResult> aggregateIntegerGroupBy(DaoConnection conn,
                                                                  Aggregation fun, String columnName, String condition,
                                                                  String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                                                  Integer limit, Integer offset) {
        String SQL = QueryBuilder.aggregate(fun.name(), tableName(), columnName, condition, groupByColumnName, having, orderBy, orderByAscDesc, limit, offset);
        if (conn == null) {
            try (DaoConnection conn2 = dao().take(false)) {
                return doFullAggregateIntegerGroupBy(conn2, SQL, groupByColumnName);
            }
        } else {
            return doFullAggregateIntegerGroupBy(conn, SQL, groupByColumnName);
        }
    }

    private List<IntegerAggregationResult> doFullAggregateIntegerGroupBy(DaoConnection conn, String SQL, String columnName) {
        Query query = conn.get().createQuery(SQL);
        return mapAggregationColumns(query, columnName).executeAndFetch(IntegerAggregationResult.class);
    }

    public List<LongAggregationResult> aggregateLongGroupBy(Aggregation fun, String columnName, String condition,
                                                            String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                                            Integer limit, Integer offset) {
        return aggregateLongGroupBy(null, fun, columnName, condition, groupByColumnName, having, orderBy, orderByAscDesc, limit, offset);
    }

    public List<LongAggregationResult> aggregateLongGroupBy(DaoConnection conn,
                                                            Aggregation fun, String columnName, String condition,
                                                            String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                                            Integer limit, Integer offset) {
        String SQL = QueryBuilder.aggregate(fun.name(), tableName(), columnName, condition, groupByColumnName, having, orderBy, orderByAscDesc, limit, offset);
        if (conn == null) {
            try (DaoConnection conn2 = dao().take(false)) {
                return doFullAggregateLongGroupBy(conn2, SQL, groupByColumnName);
            }
        } else {
            return doFullAggregateLongGroupBy(conn, SQL, groupByColumnName);
        }
    }

    private List<LongAggregationResult> doFullAggregateLongGroupBy(DaoConnection conn, String SQL, String columnName) {
        Query query = conn.get().createQuery(SQL);
        return mapAggregationColumns(query, columnName).executeAndFetch(LongAggregationResult.class);
    }

    public List<StringAggregationResult> aggregateStringGroupBy(Aggregation fun, String columnName, String condition,
                                                               String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                                               Integer limit, Integer offset) {
        return aggregateStringGroupBy(null, fun, columnName, condition, groupByColumnName, having, orderBy, orderByAscDesc, limit, offset);
    }

    public List<StringAggregationResult> aggregateStringGroupBy(DaoConnection conn,
                                                                Aggregation fun, String columnName, String condition,
                                                                String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                                                Integer limit, Integer offset) {
        String SQL = QueryBuilder.aggregate(fun.name(), tableName(), columnName, condition, groupByColumnName, having, orderBy, orderByAscDesc, limit, offset);
        if (conn == null) {
            try (DaoConnection conn2 = dao().take(false)) {
                return doFullAggregateStringGroupBy(conn2, SQL, groupByColumnName);
            }
        } else {
            return doFullAggregateStringGroupBy(conn, SQL, groupByColumnName);
        }
    }

    private List<StringAggregationResult> doFullAggregateStringGroupBy(DaoConnection conn, String SQL, String columnName) {
        Query query = conn.get().createQuery(SQL);
        return mapAggregationColumns(query, columnName).executeAndFetch(StringAggregationResult.class);
    }

    private Query mapAggregationColumns(Query query, String groupByFieldName) {
        query.addColumnMapping(groupByFieldName, "groupByValue");
        query.addColumnMapping(QueryBuilder.AGGREGATION_RESULT_COLUMN, "funResult");
        return query;
    }

    public void save(T entity) {
        save(null, entity);
    }

    public abstract void save(DaoConnection conn, T entity);

    public void delete(T entity) {
        delete(null, entity);
    }

    public abstract void delete(DaoConnection conn, T entity);

    protected long[] createOne(T entity) {
        return createOne(null, entity);
    }

    protected long[] createOne(DaoConnection conn, T entity) {
        if (conn != null) {
            return doCreateOne(conn, entity);
        } else {
            try (DaoConnection conn2 = dao().take(false)) {
                return doCreateOne(conn2, entity);
            }
        }
    }

    private long[] doCreateOne(DaoConnection conn, T entity) {
        Map<String, Object> newEntityParams = entityParameters(entity);
        String SQL = QueryBuilder.insertQuery(tableName(), newEntityParams);
        Query query = conn.get().createQuery(SQL, true);
        for (Map.Entry<String, Object> entry : newEntityParams.entrySet()) {
            query.addParameter(entry.getKey(), entry.getValue());
        }
        Object[] keys = query.executeUpdate().getKeys();
        long[] newIds = new long[keys.length];
        for (int i = 0; i < keys.length; i++) {
            newIds[i] = ((BigInteger)keys[i]).longValue();
        }
        return newIds;
    }

    public T readOne(String condition) {
        return readOne(null, condition);
    }

    public T readOne(DaoConnection conn, String condition) {
        List<T> entities = readByCondition(conn, condition);
        return entities.size() == 0 ? null : entities.get(0);
    }

    public List<T> readByField(String fieldName, long id) {
        return readByField(null, fieldName, id);
    }

    public List<T> readByField(DaoConnection conn, String fieldName, long id) {
        return readByField(conn, fieldName, id, false);
    }

    public List<T> readByField(String fieldName, long id, boolean selectDistinct) {
        return readByField(null, fieldName, id, selectDistinct);
    }

    public List<T> readByField(DaoConnection conn, String fieldName, long id, boolean selectDistinct) {
        String condition = fieldName + " = " + id;
        return readByCondition(conn, condition, selectDistinct);
    }

    public List<T> readByField(String fieldName, String text, boolean compareByLike, boolean caseInsensitive) {
        return readByField(null, fieldName, text, compareByLike, caseInsensitive);
    }

    public List<T> readByField(DaoConnection conn, String fieldName, String text, boolean compareByLike, boolean caseInsensitive) {
        return readByField(conn, fieldName, text, compareByLike, caseInsensitive, null, false);
    }

    public List<T> readByField(String fieldName, String text, boolean compareByLike, boolean caseInsensitive, String additionalCondition, boolean selectDistinct) {
        return readByField(null, fieldName, text, compareByLike, caseInsensitive, additionalCondition, selectDistinct);
    }

    public List<T> readByField(DaoConnection conn, String fieldName, String text, boolean compareByLike, boolean caseInsensitive, String additionalCondition, boolean selectDistinct) {
        String condition = buildStringFieldCondition(fieldName, text, compareByLike, caseInsensitive);
        if (additionalCondition != null) {
            condition = "(" + condition + ") AND (" + additionalCondition + ")";
        }
        return readByCondition(conn, condition, selectDistinct);
    }

    private String buildStringFieldCondition(String fieldName, String text, boolean compareByLike, boolean caseInsensitive) {
        String targetText = caseInsensitive ? text.toLowerCase() : text;
        String sqlFieldValue = QueryBuilder.sanitizeStringValue(targetText);
        String sqlFieldName = caseInsensitive ? "LOWER(" + fieldName + ")" : fieldName;
        return compareByLike ?
            sqlFieldName + " LIKE '" + sqlFieldValue + "%'" :
            sqlFieldName + " = '" + sqlFieldValue + "'";
    }

    protected List<T> readByCondition(String condition) {
        return readByCondition(null, condition);
    }

    protected List<T> readByCondition(DaoConnection conn, String condition) {
        return readByCondition(conn, condition, false);
    }

    protected List<T> readByCondition(String condition, boolean selectDistinct) {
        return readByCondition(null, condition, selectDistinct);
    }

    protected List<T> readByCondition(DaoConnection conn, String condition, boolean selectDistinct) {
        if (conn != null) {
            return doReadByCondition(conn, condition, selectDistinct);
        } else {
            try (DaoConnection conn2 = dao().take(false)) {
                return doReadByCondition(conn2, condition, selectDistinct);
            }
        }
    }

    private List<T> doReadByCondition(DaoConnection conn, String condition, boolean selectDistinct) {
        String SQL = QueryBuilder.selectQuery(tableName(), columnNames(), condition, selectDistinct);
        Query query = conn.get().createQuery(SQL);
        return mapColumns(query).executeAndFetch(getParametrizedType());
    }

    protected void updateOne(BaseEntity entity, String condition) {
        updateOne(null, entity, condition);
    }

    protected void updateOne(DaoConnection conn, BaseEntity entity, String condition) {
        if (conn != null) {
            doUpdateOne(conn, entity, condition);
        } else {
            try (DaoConnection conn2 = dao().take(false)) {
                doUpdateOne(conn2, entity, condition);
            }
        }
    }

    private void doUpdateOne(DaoConnection conn, BaseEntity entity, String condition) {
        Map<String, Object> newEntityParams = entityParameters(entity);
        String SQL = QueryBuilder.updateQuery(tableName(), newEntityParams, condition);
        Query query = conn.get().createQuery(SQL);
        for (Map.Entry<String, Object> entry : newEntityParams.entrySet()) {
            query.addParameter(entry.getKey(), entry.getValue());
        }
        mapColumns(query).executeUpdate();
    }

    protected void replaceOne(BaseEntity entity) {
        replaceOne(null, entity);
    }

    protected void replaceOne(DaoConnection conn, BaseEntity entity) {
        if (conn != null) {
            doReplaceOne(conn, entity);
        } else {
            try (DaoConnection conn2 = dao().take(false)) {
                doReplaceOne(conn2, entity);
            }
        }
    }

    private void doReplaceOne(DaoConnection conn, BaseEntity entity) {
        Map<String, Object> newEntityParams = entityParameters(entity);
        String SQL = QueryBuilder.replaceQuery(tableName(), newEntityParams);
        Query query = conn.get().createQuery(SQL);
        for (Map.Entry<String, Object> entry : newEntityParams.entrySet()) {
            query.addParameter(entry.getKey(), entry.getValue());
        }
        mapColumns(query).executeUpdate();
    }

    protected void deleteByCondition(String condition) {
        deleteByCondition(null, condition);
    }

    protected void deleteByCondition(DaoConnection conn, String condition) {
        if (conn != null) {
            doDeleteByCondition(conn, condition);
        } else {
            try (DaoConnection conn2 = dao().take(false)) {
                doDeleteByCondition(conn2, condition);
            }
        }
    }

    private void doDeleteByCondition(DaoConnection conn, String condition) {
        String SQL = QueryBuilder.deleteQuery(tableName(), condition);
        Query query = conn.get().createQuery(SQL);
        mapColumns(query).executeUpdate();
    }

    protected List<Map<String, Object>> rawRead(String SQL) {
        return rawRead(null, SQL);
    }

    protected List<Map<String, Object>> rawRead(DaoConnection conn, String SQL) {
        if (conn != null) {
            return doRawRead(conn, SQL);
        } else {
            try (DaoConnection conn2 = dao().take(false)) {
                return doRawRead(conn2, SQL);
            }
        }
    }

    private List<Map<String, Object>> doRawRead(DaoConnection conn, String SQL) {
        return conn.get().createQuery(SQL).executeAndFetchTable().asList();
    }

    private Map<String, Object> entityParameters(BaseEntity entity) {
        return entity.parameters();
    }

    private Query mapColumns(Query query) {
        for (String columnName : columnNames()) {
            query.addColumnMapping(columnName, QueryBuilder.toEntityField(columnName));
        }
        return query;
    }

    protected Class<T> getParametrizedType() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public DaoConnection transactionalConn() {
        return dao().take(true);
    }
}
