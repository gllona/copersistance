package org.logicas.librerias.copersistance.repositories;

import org.logicas.librerias.copersistance.Dao.DaoConnection;
import org.logicas.librerias.copersistance.entities.DualKeyEntity;
import org.logicas.librerias.copersistance.exceptions.CopersistenceException;

import java.util.List;
import java.util.StringJoiner;

public abstract class DualKeyEntityRepository<T extends DualKeyEntity> extends BaseEntityRepository<T> {

    protected abstract String id1FieldName();

    protected abstract String id2FieldName();

    public T readById(Long keyField1, Long keyField2) {
        return readById(null, keyField1, keyField2);
    }

    public T readById(DaoConnection conn, Long keyField1, Long keyField2) {
        if (keyField1 == null || keyField2 == null) {
            throw new CopersistenceException("Can not read with any member of dual-key entity ID == null");
        }
        String condition = id1FieldName() + " = " + keyField1 + " AND " + id2FieldName() + " = " + keyField2;
        return readOne(conn, condition);
    }

    public List<T> readByIds(List<Long[]> ids) {
        return readByIds(null, ids);
    }

    private List<T> readByIds(DaoConnection conn, List<Long[]> ids) {
        StringJoiner values = new StringJoiner("), (");
        for (Long[] idPair : ids) {
            if (idPair.length != 2) {
                throw new CopersistenceException("'ids' should be a pair when reading dual-key entities.");
            } else if (idPair[0] == null || idPair[1] == null) {
                throw new CopersistenceException("'ids' should be a pair without null's when reading dual-key entities.");
            }
            values.add(idPair[0] + ", " + idPair[1]);
        }
        return readByCondition(conn, "(" + id1FieldName() + ", " + id2FieldName() + ") IN ((" + values.toString() + "))");
    }

    public void save(DaoConnection conn, T entity) {
        if (entity.getId1() == null || entity.getId2() == null) {
            throw new CopersistenceException("Can not save a dual-key entity without its composite ID set");
        }
        replace(conn, entity);
    }

    private void update(DaoConnection conn, T entity) {
        assertIdsAreSet(entity);
        String condition = id1FieldName() + " = " + entity.getId1() + " AND " + id2FieldName() + " = " + entity.getId2();
        super.updateOne(conn, entity, condition);
    }

    private void replace(DaoConnection conn, T entity) {
        assertIdsAreSet(entity);
        super.replaceOne(conn, entity);
    }

    public void deleteById(Long id1, Long id2) {
        deleteById(null, id1, id2);
    }

    public void deleteById(DaoConnection conn, Long id1, Long id2) {
        assertIdIsSet(id1);
        assertIdIsSet(id2);
        String condition = id1FieldName() + " = " + id1 + " AND " + id2FieldName() + " = " + id2;
        super.deleteByCondition(conn, condition);
    }

    @Override
    public void delete(DaoConnection conn, T entity) {
        assertIdsAreSet(entity);
        String condition = id1FieldName() + " = " + entity.getId1() + " AND " + id2FieldName() + " = " + entity.getId2();
        super.deleteByCondition(conn, condition);
        //entity.setIds(null, null);
    }

    private void assertIdIsSet(Long id) {
        if (id == null) {
            throw new CopersistenceException("Composite ID components (key1, key2) can not be null");
        }
    }

    private void assertIdsAreSet(T entity) {
        if (entity.getId1() == null || entity.getId2() == null) {
            throw new CopersistenceException("Composite ID (key1, key2) has null values for dual-key entity");
        }
    }
}
