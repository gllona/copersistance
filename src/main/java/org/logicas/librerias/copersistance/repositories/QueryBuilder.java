package org.logicas.librerias.copersistance.repositories;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;

public class QueryBuilder {

    public static final String AGGREGATION_RESULT_COLUMN = "_aggregation";

    public static String toEntityField(String tableColumnName) {
        Matcher m = Pattern.compile("(_[A-Za-z0-9])").matcher(tableColumnName);
        StringBuffer sb = new StringBuffer();

        while (m.find()) {
            m.appendReplacement(sb, m.group().substring(1).toUpperCase());
        }
        m.appendTail(sb);

        return sb.toString();
    }

    private static String toSqlColumn(String entityColumnName) {
        Matcher m = Pattern.compile("[A-Z]").matcher(entityColumnName);
        StringBuffer sb = new StringBuffer();

        while (m.find()) {
            m.appendReplacement(sb, "_" + m.group().toLowerCase());
        }
        m.appendTail(sb);

        return sb.toString();
    }

    public static String selectQuery(String tableName, String[] columnNames, String condition, boolean selectDistinct) {
        StringJoiner sqlColumns = new StringJoiner(", ");
        for (String column : columnNames) {
            sqlColumns.add(toSqlColumn(column));
        }
        String query = "SELECT " + (selectDistinct ? "DISTINCT " : "") + sqlColumns.toString() + " FROM " + tableName + " WHERE " + condition;

        return query;
    }

    public static String count(String tableName, String condition) {
        String query = "SELECT COUNT(*) FROM " + tableName + " WHERE " + condition;

        return query;
    }

    public static String simpleAggregate(String fun, String tableName, String fieldName, String condition) {
        String query = "SELECT " + fun + "(" + fieldName + ") FROM " + tableName;

        if (condition != null) {
            query += " WHERE " + condition;
        }

        return query;
    }

    public static String aggregate(String fun, String tableName, String columnName, String condition,
                                   String groupByColumnName, String having, String orderBy, String orderByAscDesc,
                                   Integer limit, Integer offset) {
        String query = "SELECT " + groupByColumnName + ", " + fun + "(" + columnName + ") AS " + AGGREGATION_RESULT_COLUMN + " FROM " + tableName;

        if (condition != null) {
            query += " WHERE " + condition;
        }

        query += " GROUP BY " + groupByColumnName;

        if (having != null) {
            query += " HAVING " + having;
        }

        if (orderBy != null) {
            query += " ORDER BY " + orderBy;
            if (orderByAscDesc != null) {
                query += orderByAscDesc;
            }
        }

        if (limit != null) {
            query += " LIMIT " + limit;
            if (offset != null) {
                query += " OFFSET " + offset;
            }
        }

        return query;
    }

    public static String insertQuery(String tableName, Map<String, ?> params) {
        return insertOrReplaceQuery(tableName, params, false);
    }

    public static String replaceQuery(String tableName, Map<String, ?> params) {
        return insertOrReplaceQuery(tableName, params, true);
    }

    private static String insertOrReplaceQuery(String tableName, Map<String, ?> params, boolean doReplace) {
        StringBuilder columns = new StringBuilder("(");
        StringBuilder values = new StringBuilder("(");
        StringBuilder pairs = new StringBuilder("");

        Iterator<? extends Map.Entry<String, ?>> it = params.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, ?> param = it.next();

            if (it.hasNext()) {
                columns.append(param.getKey()).append(", ");
                values.append(":").append(param.getKey()).append(", ");
                pairs.append(param.getKey()).append(" = :").append(param.getKey()).append(", ");
            } else {
                columns.append(param.getKey()).append(")");
                values.append(":").append(param.getKey()).append(")");
                pairs.append(param.getKey()).append(" = :").append(param.getKey());
            }
        }

        String query = "INSERT INTO " + tableName + "" + columns + " VALUES " + values +
            (doReplace ? " ON DUPLICATE KEY UPDATE " + pairs : "");

        return query;
    }

    public static String insertQuery(String tableName, List<Map<String, Object>> params) {
        String query = "";

        if (!params.isEmpty()) {
            List<String> columnsNames = new ArrayList<>();
            StringBuilder columns = new StringBuilder(" (");
            StringBuilder valuesList = new StringBuilder("");

            Iterator<? extends Map.Entry<String, ?>> it = params.get(0).entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, ?> param = it.next();
                columnsNames.add(param.getKey());

                if (it.hasNext()) {
                    columns.append(param.getKey()).append(", ");
                } else {
                    columns.append(param.getKey()).append(")");
                }
            }

            String separator = "";

            for (Map<String, Object> param : params) {
                valuesList.append(separator);
                separator = ",";
                StringBuilder values = new StringBuilder("(");
                Iterator<String> columnIterator = columnsNames.iterator();

                while (columnIterator.hasNext()) {
                    String columnName = columnIterator.next();

                    if (columnIterator.hasNext()) {
                        values.append("\'").append(param.get(columnName)).append("\'").append(", ");
                    } else {
                        values.append("\'").append(param.get(columnName)).append("\'").append(")");
                    }
                }

                valuesList.append(values);
            }

            query = "INSERT INTO " + tableName + "" + columns + " VALUES " + valuesList;
        }

        return query;
    }

    public static String updateQuery(String tableName, Map<String, ?> params, String condition) {
        StringBuilder updates = new StringBuilder(" ");

        Iterator<? extends Map.Entry<String, ?>> it = params.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, ?> param = it.next();

            if (it.hasNext()) {
                updates.append(param.getKey()).append(" = :").append(param.getKey()).append(", ");
            } else {
                updates.append(param.getKey()).append(" = :").append(param.getKey()).append(" ");
            }
        }

        String query = "UPDATE " + tableName + " SET " + updates + " WHERE " + condition;

        return query;
    }

    public static String updateQuery(String tableName, List<Map<String, Object>> params, String keyColumnNames[]) {
        String query = "";

        if (!params.isEmpty()) {
            StringBuilder updates = new StringBuilder("");
            Iterator<? extends Map.Entry<String, ?>> it = params.get(0).entrySet().iterator();

            while (it.hasNext()) {
                Map.Entry<String, ?> column = it.next();
                updates.append(column.getKey()).append(" = (");
                updates.append(createCaseThenConditions(params, keyColumnNames, column.getKey()));
                updates.append(")");
                if (it.hasNext()) {
                    updates.append(", ");
                }
            }

            query = "UPDATE " + tableName + " SET " + updates + " " + createWhereInCondition(keyColumnNames, params);
        }

        return query;
    }

    public static String deleteQuery(String tableName, String condition) {
        String query = "DELETE FROM " + tableName + " WHERE " + condition;

        return query;
    }

    public static String sanitizeStringValue(String value) {
        return escapeSql(value);
    }

    public static String escapeSql(String str) {
        if (str == null) {
            return null;
        }
        return StringUtils.replace(str, "'", "''");
    }

    public static String listToSqlString(List<?> list) {
        StringBuilder builder = new StringBuilder();
        String separator = "";
        builder.append("(");

        for (Object value : list) {
            builder.append(separator + value.toString());
            separator = ",";
        }

        builder.append(")");

        return list.isEmpty() ? "" : builder.toString();
    }

    private static String createCaseThenConditions(List<Map<String, Object>> params, String keyColumnNames[], Object key) {
        String query = "CASE\n";

        for (Map<String, Object> param : params) {
            query += "WHEN ";
            String connector = "";

            for (String keyColumn : keyColumnNames) {
                query += connector;
                connector = " AND ";
                query += keyColumn + " = " + param.get(keyColumn);
            }

            Object value = param.get(key);
            String isDate = value instanceof Timestamp ? "::TIMESTAMP" : "";   //TODO convert postgres to mysql

            query += " THEN " + needQuote(value) + param.get(key) + needQuote(value) + isDate + "\n";
        }

        query += "END";

        return query;
    }

    private static String createWhereInCondition(String keyColumnNames[], List<Map<String, Object>> params) {
        String query = "WHERE (";

        for (int i = 0; i < keyColumnNames.length; i++) {
            if (i > 0) {
                query += ", ";
            }

            query += keyColumnNames[i];
        }

        query += ") in (";
        Iterator<Map<String, Object>> it = params.iterator();

        while (it.hasNext()) {
            Map<String, Object> param = it.next();
            query += "(";
            String separator = "";

            for (String keyColumn : keyColumnNames) {
                query += separator;
                separator = ", ";

                query += param.get(keyColumn);
            }
            query += ")";

            if (it.hasNext()) {
                query += ", ";
            }
        }

        query += ")";

        return query;
    }

    private static String needQuote(Object value) {
        return value instanceof String || value instanceof Timestamp ? "\'" : "";
    }
}
