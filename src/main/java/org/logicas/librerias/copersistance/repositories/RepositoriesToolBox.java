package org.logicas.librerias.copersistance.repositories;

import org.logicas.librerias.copersistance.Settings;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.StringJoiner;
import java.util.TimeZone;

public class RepositoriesToolBox {
    public enum InNotInStatesFilter {IN, NOT_IN}

    public enum AscDescOrder {ASC, DESC}

    public static Date sqlDateToJavaDate(String sqlDate) throws ParseException {
        SimpleDateFormat df = new SimpleDateFormat(sqlDate.length() > 10 ? "yyyy-MM-dd HH:mm:ss" : "yyyy-MM-dd");
        return df.parse(sqlDate);
    }

    public static java.sql.Timestamp sqlTimestamp(Date date) {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        df.setTimeZone(TimeZone.getTimeZone(Settings.TIMEZONE));

        return java.sql.Timestamp.valueOf(df.format(date));
    }

    public static java.sql.Timestamp sqlCurrentTimestamp() {
        return sqlTimestamp(new Date());
    }

    public static String sqlWhereInExprForIntegers(List<Integer> list) {
        StringJoiner sj = new StringJoiner(", ");
        for (Integer elem : list) {
            sj.add(elem.toString());
        }
        return "(" + sj.toString() + ")";
    }

    public static String sqlWhereInExprForLongs(List<Long> list) {
        StringJoiner sj = new StringJoiner(", ");
        for (Long elem : list) {
            sj.add(elem.toString());
        }
        return "(" + sj.toString() + ")";
    }

    public static String sqlWhereInExprForStrings(List<String> list) {
        StringJoiner sj = new StringJoiner("', '");
        for (String elem : list) {
            sj.add(QueryBuilder.sanitizeStringValue(elem));
        }
        return "('" + sj.toString() + "')";
    }

    public static String sqlExprForString(String elem) {
        return "'" + QueryBuilder.sanitizeStringValue(elem) + "'";
    }
}
