package org.logicas.librerias.copersistance.repositories;

import org.logicas.librerias.copersistance.Dao.DaoConnection;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;
import org.logicas.librerias.copersistance.exceptions.CopersistenceException;

import java.util.List;
import java.util.StringJoiner;

public abstract class SingleKeyEntityRepository<T extends SingleKeyEntity> extends BaseEntityRepository<T> {

    protected abstract String idFieldName();

    public T readById(Long id) {
        return readById(null, id);
    }

    public T readById(DaoConnection conn, Long id) {
        if (id == null) {
            throw new CopersistenceException("Can not read with id == null");
        }
        String condition = idFieldName() + " = " + id;
        return readOne(conn, condition);
    }

    public List<T> readByIds(List<Long> ids) {
        return readByIds(null, ids);
    }

    public List<T> readByIds(DaoConnection conn, List<Long> ids) {
        StringBuilder condition = new StringBuilder(idFieldName() + " IN (");
        StringJoiner values = new StringJoiner(", ");
        for (Long id : ids) {
            if (id != null) {
                values.add(String.valueOf(id));
            }
        }
        condition.append(values.toString()).append(")");
        return readByCondition(conn, condition.toString());
    }

    @Override
    public void save(DaoConnection conn, T entity) {
        if (entity.getId() == null) {
            long[] newId = createOne(conn, entity);
            entity.setId(newId[0]);
        } else {
            update(conn, entity);
        }
    }

    private void update(DaoConnection conn, T entity) {
        assertIdIsSet(entity);
        String condition = idFieldName() + " = " + entity.getId();
        super.updateOne(conn, entity, condition);
    }

    public void deleteById(Long id) {
        deleteById(null, id);
    }

    public void deleteById(DaoConnection conn, Long id) {
        assertIdIsSet(id);
        String condition = idFieldName() + " = " + id;
        super.deleteByCondition(conn, condition);
    }

    @Override
    public void delete(DaoConnection conn, T entity) {
        assertIdIsSet(entity);
        String condition = idFieldName() + " = " + entity.getId();
        super.deleteByCondition(conn, condition);
        //entity.setId(null);
    }

    private void assertIdIsSet(Long id) {
        if (id == null) {
            throw new CopersistenceException("ID can not be null");
        }
    }

    private void assertIdIsSet(T entity) {
        if (entity.getId() == null) {
            throw new CopersistenceException("Id not set for entity");
        }
    }
}
