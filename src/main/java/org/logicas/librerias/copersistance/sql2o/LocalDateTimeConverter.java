package org.logicas.librerias.copersistance.sql2o;

import org.sql2o.converters.Converter;
import org.sql2o.converters.ConverterException;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateTimeConverter implements Converter<LocalDateTime> {
    @Override
    public LocalDateTime convert(final Object val) throws ConverterException {
        if (val instanceof java.sql.Timestamp) {
            return ((java.sql.Timestamp) val).toLocalDateTime();
        } else {
            return null;
        }
    }

    @Override
    public Object toDatabaseParam(final LocalDateTime val) {
        if (val == null) {
            return null;
        } else {
            return new java.sql.Timestamp(val.toInstant(ZoneOffset.systemDefault().getRules().getOffset(Instant.now())).toEpochMilli());
        }
    }
}
